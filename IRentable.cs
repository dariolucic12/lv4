﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak3i4
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
