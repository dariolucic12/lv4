﻿using System;
using System.Collections.Generic;

namespace zadatak3i4
{
    class Program
    {
        static void Main(string[] args)
        {
            Book warAndPiece = new Book("War and Piece");
            Video inception = new Video("Inception");
            List<IRentable> RentableList = new List<IRentable>();

            RentableList.Add(warAndPiece);
            RentableList.Add(inception);

            RentingConsolePrinter printer = new RentingConsolePrinter();

            printer.DisplayItems(RentableList);             //zadatak3
            printer.PrintTotalPrice(RentableList);

            Book cinderella = new Book("Cinderella");
            Video theGodfather = new Video("The Godfather");

            HotItem item1 = new HotItem(cinderella);
            HotItem item2 = new HotItem(theGodfather);

            RentableList.Add(item1);
            RentableList.Add(item2);

            printer.DisplayItems(RentableList);         
            printer.PrintTotalPrice(RentableList);          //zadatak4

        }
    }
}
